import { Injectable } from '@angular/core';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private users: User[] = [
    { id: 1, username: 'jokarvu', email: 'jokarvu@gmail.com', password: 'banc' },
    { id: 2, username: 'jokarvu2', email: 'jokarvu2@gmail.com', password: 'banc' },
    { id: 3, username: 'jokarvu3', email: 'jokarvu3@gmail.com', password: 'banc' },
  ];
  constructor() { }

  getUsers(): User[] {
    return this.users;
  }
}
