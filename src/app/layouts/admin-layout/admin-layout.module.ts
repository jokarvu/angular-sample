import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AdminLayoutRoutes } from './admin-layout-routing.module';
import { UserIndexComponent } from '../../pages/user/user-index/user-index.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    HttpClientModule
  ],
  declarations: [
    UserIndexComponent
  ]
})

export class AdminLayoutModule {}
