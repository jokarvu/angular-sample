import { Routes } from '@angular/router';
import { UserIndexComponent } from '../../pages/user/user-index/user-index.component';

export const AdminLayoutRoutes: Routes = [
  { path: 'user', component: UserIndexComponent}
];
